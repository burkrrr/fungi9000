#!/usr/bin/env node

const inquirer = require("inquirer");
const fs = require('fs');

const init = () => {
  console.log('------------------\nWelcome!\n------------------\n')
};

const askQuestions = () => {
  const questions = [
    {
      name: "NAME",
      type: "input",
      message: "What is your name?"
    },
    {
      name: "THINGY",
      type: "input",
      message: "Would you like to see a thing?"
    },
    {
      name: "VIEW",
      type: "input",
      message: "Please hit <enter> to continue."
    },
  ];
  return inquirer.prompt(questions);
};

const greeting = (name) => {
  console.log(
    `\n*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#\n\n`,
    `Hello there ${name}!\n`,
    `I assume you answered in the affirmative to wanting to see a thing. If not, then... oops.\n`,
    `Now, prepare to be whelmed at the appropriate level!\n`,
    `\n*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#\n`
    )
}

const showImg = () => {
    fs.readFile('data/mushrooms.txt', 'utf8', function (err, artwork) {
      if (err) {
        return error(`Error. PANIC!`)
      }
        console.log(artwork);
        console.log(
          `\n\n--------------------------------------------\n\n`,
          `Sorry you had to scroll to see the picture. I will do better next time. OK that is all, thank you, bye.`,
          `\n\n--------------------------------------------\n\n`
          )
    }
  )
};

const run = async () => {
  // show script introduction
  init();

  // ask questions
  const answers = await askQuestions();
  const { NAME } = answers;
  greeting(NAME);
  
  // messages
  setTimeout(() => {
    console.log(
      `\n\n--------------------------------------------\n\n`,
      `Wait for it.... this is going to be AMAZING!`,
      `\n\n--------------------------------------------\n\n`
      )
  },3000)

  setTimeout(() => {
    console.log(
      `\n\n--------------------------------------------\n\n`,
      `Truly, a wonder for all the senses!`,
      `\n\n--------------------------------------------\n\n`
      )
  },5000)

  //show image
  setTimeout(function() {
    showImg()
    }, 8000);
};

run();
