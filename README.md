# ASCII Art

Use your terminal of choice to make all your dreams come true and be entertained for at least 1 minute.

## Get Started

1. Clone this repo `git clone https://burkrrr@bitbucket.org/burkrrr/fungi9000.git`
2. Navigate the folder and install `cd fungi9000 && npm install`
3. Begin! `npm start`



Tada! 

The End.

